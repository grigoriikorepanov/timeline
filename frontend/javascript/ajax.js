const ajax = () => {
  return new Promise((request, reject) => {
    const xhr = new XMLHttpRequest();

    xhr.open('GET', '../timeline.json', false);
    xhr.send();

    if(xhr.status !== 200) {
      reject(xhr.status + ': ' + xhr.statusText);
    } else {
      request(xhr.responseText);
    }
  });
};

// даже не знаю что и зачем