class Timeline {
  constructor(itemId, json) {
    this.timeline = document.querySelector(itemId);
    this.timeline.classList.add("timeline");
    this.initSortingButtons();
    this.initButton = true;

    this.activeEvent = null;
    this.queue = [];

    this.queue = JSON.parse(json).map(event => {
      return this.parseEvents(event);
    });
    this.sortByDateAndShow();
    this.init();

    // ajax().then(request => { // Немного не понял почему проект перестал работать в один из моментов, решил пока убрать
    //   data = JSON.parse(request);
    //
    //   this.queue = data.map(event => {
    //     return this.parseEvents(event);
    //   });
    //
    //   this.sortByDateAndShow();
    //   this.init();
    //
    // }).catch(error => {
    //   console.log(error);
    // });
  }

  static sortDate(...events) {
    return events[0].date - events[1].date;
  }

  static sortType(...events) {
    return events[0].date - events[1].date;
  }

  sortByDateAndShow() {
    this.queue.sort((...events) => events[0].date - events[1].date);
    this.init();
  }

  sortByReversDateAndShow() {
    this.queue.sort((...events) => -(events[0].date - events[1].date));
    this.init();
  }

  sortByTypeAndShow() {
    this.queue.sort((...events) => {
      if(events[0].type > events[1].type) {
        return 1;
      } else if (events[0].type < events[1].type) {
        return -1;
      } else return 0;
    });
    this.init();
  }

  sortByReversTypeAndShow() {
    this.queue.sort((...events) => {
      if(events[0].type > events[1].type) {
        return -1;
      } else if (events[0].type < events[1].type) {
        return 1;
      } else return 0;
    });
    this.init();
  }

  pushEvent(event) {
    const item = this.parseEvents(event);
    this.queue.push(item);
    this.timeline.appendChild(item.block);
  }

  init() {
    this.queue.forEach(item => {
      this.timeline.appendChild(item.block);
    });
  }

  initSortingButtons() {
    if(!this.initButton) {
      const blockButtonsSort = document.createElement("div");
      blockButtonsSort.classList.add("timeline__sort");

      const buttonSortDate = document.createElement("div");
      const buttonSortType = document.createElement("div");

      buttonSortDate.classList.add("timeline__sort-button");
      buttonSortDate.textContent = "Sort by date";
      buttonSortDate.setAttribute("data-sort", "bottom");
      buttonSortDate.addEventListener("click", () => {
        buttonSortType.classList.remove("active");
        buttonSortDate.classList.add("active");
        if(buttonSortDate.dataset.sort === "top") {
          this.sortByDateAndShow();
          buttonSortDate.dataset.sort = "bottom";
        } else {
          this.sortByReversDateAndShow();
          buttonSortDate.dataset.sort = "top";
        }
      });

      buttonSortType.classList.add("timeline__sort-button");
      buttonSortType.textContent = "Sort by type";
      buttonSortType.setAttribute("data-sort", "top");
      buttonSortType.addEventListener("click", () => {
        buttonSortDate.classList.remove("active");
        buttonSortType.classList.add("active");
        if(buttonSortType.dataset.sort === "top") {
          this.sortByTypeAndShow();
          buttonSortType.dataset.sort = "bottom";
        } else {
          this.sortByReversTypeAndShow();
          buttonSortType.dataset.sort = "top";
        }
      });

      blockButtonsSort.appendChild(buttonSortDate);
      blockButtonsSort.appendChild(buttonSortType);

      this.timeline.appendChild(blockButtonsSort);
    } else {
      console.log("Кнопки уже добавлены");
    }
  }

  parseEvents(event) {
    const events = {
      "transaction": (data) => { return this.eventTransaction(data); },
      "news": (data) => { return this.eventNews(data); },
    };
    return events[event.type](event);
  }

  eventTransaction(transaction) {
    const blockTransaction = document.createElement("section");
    blockTransaction.className = "timeline__item transaction";

    blockTransaction.addEventListener("click", (e) => this.handleClickOnTheTimeline(e, blockTransaction));

    const itemBasic = document.createElement("div");
    itemBasic.classList.add("item__basic");

    const transactionInfo = document.createElement("div");
    transactionInfo.classList.add("transaction__info");

    const fromWhom = document.createElement("h3");
    fromWhom.classList.add("title");
    fromWhom.textContent = transaction.from;

    const date = document.createElement("div");
    date.textContent = transaction.date;

    transactionInfo.appendChild(fromWhom);
    transactionInfo.appendChild(date);

    const amount = document.createElement("div");
    amount.textContent = transaction.amount;
    amount.classList.add(`transaction__amount`);
    amount.classList.add(`transaction__amount-${transaction.status}`);

    const parseCurrency = {
      "dollar": "$",
      "ruble": "₽",
      "euro": "€",
    };

    const currency = document.createElement("span");
    currency.classList.add(`amount__currency`);
    currency.textContent = parseCurrency[transaction.currency];
    amount.appendChild(currency);

    itemBasic.appendChild(transactionInfo);
    itemBasic.appendChild(amount);

    const itemExtra = document.createElement("div");
    itemExtra.classList.add("item__extra");

    const description = document.createElement("p");
    description.classList.add("description");
    description.textContent = transaction.description;

    const itemExtraButtons = document.createElement("div");
    itemExtraButtons.classList.add("buttons");

    const buttonDeleteTransaction = document.createElement("div");
    buttonDeleteTransaction.classList.add("transaction__extra-delete");
    buttonDeleteTransaction.classList.add("button");
    buttonDeleteTransaction.textContent = "Удалить";
    buttonDeleteTransaction.addEventListener("click", () => {
      for(let i=0;i < this.queue.length; i++) {
        if(this.queue[i].block === blockTransaction) { // И тут я понимаю что лучше бы использовал Map
          this.queue.splice(i,1);
          blockTransaction.remove();
          break;
        }
      }
    });


    itemExtraButtons.appendChild(buttonDeleteTransaction);

    itemExtra.appendChild(description);
    itemExtra.appendChild(itemExtraButtons);


    blockTransaction.appendChild(itemBasic);
    blockTransaction.appendChild(itemExtra);


    return {
      block: blockTransaction,
      type: transaction.type,
      date: +new Date(transaction.date),
    };
  }

  eventNews(news) {
    const blockNews = document.createElement("section");
    blockNews.className = "timeline__item news";

    blockNews.addEventListener("click", (e) => this.handleClickOnTheTimeline(e, blockNews));

    const itemBasic = document.createElement("div");
    itemBasic.classList.add("item__basic");

    const title = document.createElement("h3");
    title.classList.add("title");
    title.textContent = news.title;

    itemBasic.appendChild(title);

    const itemExtra = document.createElement("div");
    itemExtra.classList.add("item__extra");

    const description = document.createElement("p");
    description.classList.add("description");
    description.textContent = news.description;

    itemExtra.appendChild(description);

    const itemExtraButtons = document.createElement("div");
    itemExtraButtons.classList.add("buttons");

    const buttonIntroducedNews = document.createElement("div");
    buttonIntroducedNews.classList.add("news__extra-introduced");
    buttonIntroducedNews.classList.add("button");
    buttonIntroducedNews.textContent = "Ознакомлен";
    buttonIntroducedNews.addEventListener("click", () => {
      blockNews.classList.add("news--introduced");
      buttonIntroducedNews.remove();
    });


    itemExtraButtons.appendChild(buttonIntroducedNews);
    itemExtra.appendChild(itemExtraButtons);


    blockNews.appendChild(itemBasic);
    blockNews.appendChild(itemExtra);

    return {
      block: blockNews,
      type: news.type,
      date: +new Date(news.date),
    };
  }

  handleClickOnTheTimeline (e, block) {
    if(!e.target.classList.contains("button")) {
      this.activeEvent && this.activeEvent !== block && this.activeEvent.classList.remove("active");
      this.activeEvent = block;
      block.classList.toggle("active");
    }
  }
}

const timelineJson = `[
  {
    "amount": 300.00,
    "currency": "dollar",
    "date":"02.02.2019",
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    "from": "Марк Туллий Цицерон Марк Туллий Цицерон Марк Туллий Цицерон",
    "status":"income",
    "type":"transaction"
  },
  {
    "amount": 100.00,
    "currency": "dollar",
    "date":"01.02.2018",
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    "from": "Марк Туллий Цицерон",
    "status":"expense",
    "type":"transaction"
  },
  {
    "amount": 1300.00,
    "currency": "dollar",
    "date":"03.01.2018",
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    "from": "Марк Туллий Цицерон",
    "status":"expense",
    "type":"transaction"
  },
  {
    "date":"01.02.2018",
    "description": "Подписание кредитного договора на выгодных условиях вовсе не является гарантией стабильности. Параметры заимствования могут быть изменены банком при изменении экономической обстановки в целом стране, в отрасли или в семье конкретного клиента. Однажды заемщику может просто прийти уведомление о корректировке условий договора, чаще всего, связанной с увеличением процентной ставки.",
    "title": "Повышение процентов по кредитам",
    "type":"news",
    "readed": false
  },
  {
    "date":"01.01.2018",
    "description": "Подписание кредитного договора на выгодных условиях вовсе не является гарантией стабильности. Параметры заимствования могут быть изменены банком при изменении экономической обстановки в целом стране, в отрасли или в семье конкретного клиента. Однажды заемщику может просто прийти уведомление о корректировке условий договора, чаще всего, связанной с увеличением процентной ставки.",
    "title": "Повышение процентов по кредитам",
    "type":"news",
    "readed": false
  },
  {
    "date":"01.01.2018",
    "description": "Подписание кредитного договора на выгодных условиях вовсе не является гарантией стабильности. Параметры заимствования могут быть изменены банком при изменении экономической обстановки в целом стране, в отрасли или в семье конкретного клиента. Однажды заемщику может просто прийти уведомление о корректировке условий договора, чаще всего, связанной с увеличением процентной ставки.",
    "title": "Повышение процентов по кредитам Повышение процентов по кредитам Повышение процентов по кредитам",
    "type":"news",
    "readed": false
  },
  {
    "date":"12.01.2018",
    "description": "Подписание кредитного договора на выгодных условиях вовсе не является гарантией стабильности. Параметры заимствования могут быть изменены банком при изменении экономической обстановки в целом стране, в отрасли или в семье конкретного клиента. Однажды заемщику может просто прийти уведомление о корректировке условий договора, чаще всего, связанной с увеличением процентной ставки.",
    "title": "Повышение процентов по кредитам",
    "type":"news",
    "readed": false
  },
  {
    "date":"01.03.2018",
    "description": "Подписание кредитного договора на выгодных условиях вовсе не является гарантией стабильности. Параметры заимствования могут быть изменены банком при изменении экономической обстановки в целом стране, в отрасли или в семье конкретного клиента. Однажды заемщику может просто прийти уведомление о корректировке условий договора, чаще всего, связанной с увеличением процентной ставки.",
    "title": "Повышение процентов по кредитам",
    "type":"news",
    "readed": false
  },
  {
    "date":"01.04.2018",
    "description": "Подписание кредитного договора на выгодных условиях вовсе не является гарантией стабильности. Параметры заимствования могут быть изменены банком при изменении экономической обстановки в целом стране, в отрасли или в семье конкретного клиента. Однажды заемщику может просто прийти уведомление о корректировке условий договора, чаще всего, связанной с увеличением процентной ставки.",
    "title": "Повышение процентов по кредитам",
    "type":"news",
    "readed": false
  },
  {
    "date":"12.31.2018",
    "description": "Подписание кредитного договора на выгодных условиях вовсе не является гарантией стабильности. Параметры заимствования могут быть изменены банком при изменении экономической обстановки в целом стране, в отрасли или в семье конкретного клиента. Однажды заемщику может просто прийти уведомление о корректировке условий договора, чаще всего, связанной с увеличением процентной ставки.",
    "title": "Повышение процентов по кредитам",
    "type":"news",
    "readed": false
  },
  {
    "amount": 1000.00,
    "currency": "dollar",
    "date":"01.05.2018",
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    "from": "Марк Туллий Цицерон",
    "status":"expense",
    "type":"transaction"
  },
  {
    "amount": 1000.00,
    "currency": "dollar",
    "date":"01.06.2018",
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    "from": "Марк Туллий Цицерон",
    "status":"expense",
    "type":"transaction"
  },
  {
    "amount": 1000.00,
    "currency": "dollar",
    "date":"01.07.2018",
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    "from": "Марк Туллий Цицерон",
    "status":"expense",
    "type":"transaction"
  },
  {
    "amount": 1000.00,
    "currency": "dollar",
    "date":"01.08.2018",
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    "from": "Марк Туллий Цицерон",
    "status":"expense",
    "type":"transaction"
  },
  {
    "amount": 1000.00,
    "currency": "dollar",
    "date":"01.09.2018",
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    "from": "Марк Туллий Цицерон",
    "status":"expense",
    "type":"transaction"
  },
  {
    "amount": 1000.00,
    "currency": "ruble",
    "date":"01.10.2018",
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    "from": "Марк Туллий Цицерон",
    "status":"expense",
    "type":"transaction"
  },
  {
    "amount": 1000.00,
    "currency": "euro",
    "date":"09.20.2018",
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    "from": "Марк Туллий Цицерон",
    "status":"expense",
    "type":"transaction"
  }
]`;

const timeline = new Timeline("#app", timelineJson);